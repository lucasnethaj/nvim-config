func! WordProcessor()
  " movement changes
	map <silent> <leader>o :!openaspdf %<cr><cr>
	map <silent> <leader>c :!compiletopdf %<cr><cr>
  map j gj
  map k gk
  map $ g$
  " formatting text
  setlocal formatoptions=1
  setlocal noexpandtab
  setlocal wrap
  setlocal linebreak
  setlocal spell spelllang=en

  map <S-CR> :NotebookEvaluate<CR>
  map <C-CR> :NotebookEvaluateAll<CR>

  fu! Compile()
    pandoc -tpdf -o %:r.pdf %
  endfu
endfu
com! WP call WordProcessor()

autocmd FileType markdown WP

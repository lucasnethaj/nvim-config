--  ____  _             _
-- |  _ \| |_   _  __ _(_)_ __  ___
-- | |_) | | | | |/ _` | | '_ \/ __|
-- |  __/| | |_| | (_| | | | | \__ \
-- |_|   |_|\__,_|\__, |_|_| |_|___/
--                |___/
--
-- Bootstrap packer
local fn = vim.fn
local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
	packer_bootstrap = fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
	vim.cmd [[packadd packer.nvim]]
end

-- Automatically regenerate compiled loader file when this file is changed
vim.cmd([[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerCompile
  augroup end
]])

return require('packer').startup(function(use)
	-- Let Packer manage itself
	use 'wbthomason/packer.nvim'

	-- Installed Plugins --

	-- Language integration
	use { 'williamboman/mason.nvim', config = function() require("mason").setup() end }
	use 'williamboman/mason-lspconfig.nvim'
	use 'neovim/nvim-lspconfig'

	-- Completion
	use 'hrsh7th/nvim-cmp'
	use 'hrsh7th/cmp-nvim-lsp'
	use 'hrsh7th/cmp-vsnip'
	use 'hrsh7th/vim-vsnip'

	-- Treesitter
	use 'nvim-treesitter/nvim-treesitter'
	use 'nvim-treesitter/playground'
	-- use 'nvim-treesitter/nvim-treesitter-refactor'
	use "nvim-treesitter/nvim-treesitter-context"
	use 'windwp/nvim-ts-autotag'
	use 'p00f/nvim-ts-rainbow'
	use 'JoosepAlviste/nvim-ts-context-commentstring'
	use 'nvim-treesitter/nvim-treesitter-textobjects'

	-- Utilities
	use 'tpope/vim-fugitive'
	use 'tpope/vim-commentary'
	use 'tpope/vim-surround'
	use 'junegunn/vim-easy-align'
	use 'nvim-telescope/telescope.nvim'
	use 'gpanders/editorconfig.nvim'
	use ({
		"iamcco/markdown-preview.nvim",
		run = function() vim.fn["mkdp#util#install"]() end,
	})
	use {
		'lewis6991/gitsigns.nvim',
		config = function()
			require('gitsigns').setup()
		end
	}

	-- Cosmetics
	use 'ellisonleao/gruvbox.nvim'
	use { 'ojroques/nvim-hardline', config = function() require("hardline").setup {} end }
	use {
		"folke/todo-comments.nvim",
		requires = "nvim-lua/plenary.nvim",
		config = function()
			require("todo-comments").setup {
				-- your configuration comes here
			}
		end
	}
	use {'lukas-reineke/indent-blankline.nvim'}

	-- Libraries
	use 'nvim-lua/plenary.nvim'

	if packer_bootstrap then
		require('packer').sync()
	end
end)

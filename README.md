My Neovim configuration
Config is made for neovim 0.8.1 release, other major or minor are unlikely to work.

Install:

```bash
$ git clone https://codeberg.org/lucasnethaj/nvim-config ~/.config/nvim
```

When you first open neovim it will bootstrap the package manager 
and install lsp and treesitter support for a couple of languages.
Some of which will fail if you don't have the language specific tools (eg. cargo)  
The automatically installed languages can be configured in [lua/lsp-config.lua](lua/lsp-config.lua)
and [lua/treesitter.lua](lua/treesitter.lua).

The treesitter cli is also needed to compile the grammar files for some languages (eg. d)
If your distribution doesn't provide treesitter then you can install it from npm with:

```bash
# npm i -g tree-sitter-cli
```

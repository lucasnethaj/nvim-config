"
"  ___       _ _         _
" |_ _|_ __ (_) |___   _(_)_ __ ___
"  | || '_ \| | __\ \ / / | '_ ` _ \
"  | || | | | | |_ \ V /| | | | | | |
" |___|_| |_|_|\__(_)_/ |_|_| |_| |_|
"

runtime lua/plugins.lua
" Other configuration files are loaded via for their respective plugins via packer
" load filetype plugins
runtime ftplugin/*
" anything that neeeds to be loaded after plugins goes in the after dir.
runtime after/*.vim
runtime after/lua/*.lua
runtime lua/lsp-config.lua
runtime lua/treesitter.lua
runtime lua/cmp-config.lua

"HACK for fixing TSrainbow
" autocmd BufWritePost * TSDisable rainbow | TSEnable rainbow

" Universal Statusline
set laststatus=3
" set winbar item: help, modified, file, quickfix list
set winbar=%h%m%t
" set wildmode=longest,list,full
" Unify clipboard
set clipboard+=unnamedplus
set number
set relativenumber
" Enable mouse
set mouse=a
" Set swapdir
set dir=~/.local/tmp
set undofile
set hidden

" Set markdown preview to firefox
let g:mkdp_browser = 'firefox'

" smartcase for search
set ignorecase smartcase

" show some characters for whitespace
set list
set listchars=tab:>-,lead:·,trail:·
set tabstop=4 shiftwidth=4
" remove trailing whitespace on save
" autocmd BufWritePre * :%s/\s\+$//e
set wrap
set linebreak
" note trailing space at end of next line
set showbreak=>\ \

let mapleader = (" ")

" Exit terminal mode with ESC
tnoremap <Esc> <C-\><C-n>

" Tab to open completion menu
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" ctrl+s to save
map <C-s> :write<CR>
inoremap <C-s> <Esc>:write<CR>a

" Allow gf to follow files that don't exist
map gf :edit <cfile><cr>

" Execute Current file
map <leader>x :!./%<CR>
" map <S-CR> :!./%<CR>

" Splits & tabs
map <leader>wn :tabnew<CR>:e ./<CR>
map <leader>ws :split<CR>:e ./<CR>
map <leader>wv :vsplit<CR>:e ./<CR>

" Use ctrl-[hjkl] to select the active split!
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-l> :wincmd l<CR>
tnoremap <c-h> <C-\><C-N><C-w>h
tnoremap <c-j> <C-\><C-N><C-w>j
tnoremap <c-k> <C-\><C-N><C-w>k
tnoremap <c-l> <C-\><C-N><C-w>l
inoremap <c-h> <C-\><C-N><C-w>h
inoremap <c-j> <C-\><C-N><C-w>j
inoremap <c-k> <C-\><C-N><C-w>k
inoremap <c-l> <C-\><C-N><C-w>l

"Navigate
noremap <C-A-j> :bnext<CR>
noremap <C-A-k> :bprevious<CR>
noremap <C-A-l> :tabnext<CR>
noremap <C-A-h> :tabprevious<CR>

"Move lines
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

noremap <A-l> xp
nnoremap <A-h> xhP

" Resource init.vim
map <leader>vr :so $XDG_CONFIG_HOME/nvim/init.vim<CR>

" Lsp Control
map <leader>lr :LspRestart<CR>
map <leader>ld :LspStop<CR>
map <leader>ls :LspStart<CR>
map <leader>li :LspInfo<CR>
map <leader>lI :LspInstall

" Telescope Maps
map <leader>tt :Telescope<CR>
map <leader>ff :Telescope find_files<CR>
map <leader>tb :Telescope buffers<CR>
map <leader>tg :Telescope live_grep<CR>
map <leader>ts :Telescope lsp_document_symbols<CR>
map <leader>td :Telescope diagnostics<CR>

" Git Maps
map <leader>gg :Git<CR>
map <leader>gb :Git blame<CR>

noremap Q <ESC>

" Make help windows open in vertical split
autocmd FileType help wincmd L

" Terminal fixes
func! TerminalOptions()
  startinsert
  setlocal nonumber
  setlocal norelativenumber
endfu
autocmd TermOpen * call TerminalOptions()

" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

com! RemoveTrailing %s/\s\+$//e

au TextYankPost * silent! lua vim.highlight.on_yank {on_visual=false}

" vim: fdm=marker ts=2 sts=2 sw=2 fdl=0:

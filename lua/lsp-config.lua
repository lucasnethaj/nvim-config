local ok, _ = pcall(require, "mason-lspconfig")
if ok then
	_.setup(
		{
			-- See available servers with :LspInstall <tab>
			ensure_installed = { "rust_analyzer", "sumneko_lua", "serve_d", "clangd", "tsserver" }
		}
	)
end
if not ok then
	print("mason not loaded yet, you may need to restart neovim");
end

local vim = vim
local keymap = vim.keymap.set;
local opts = { noremap = true, silent = true }
keymap('n', '<space>e', vim.diagnostic.open_float, opts)
keymap('n', '[e', vim.diagnostic.goto_prev, opts)
keymap('n', ']e', vim.diagnostic.goto_next, opts)
-- keymap('n', '<space>q', vim.diagnostic.setloclist, opts) -- Use an on_attach function to only map thefollowing keys

-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)

	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	-- Mappings.
	local lspfunc = vim.lsp.buf;
	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	keymap('n', 'gO', '<cmd>SymbolsOutline<CR>', bufopts)
	keymap('n', 'gD', lspfunc.declaration, bufopts)
	keymap('n', 'gd', lspfunc.definition, bufopts)
	keymap('n', 'K', lspfunc.hover, bufopts)
	keymap('n', 'gi', lspfunc.implementation, bufopts)
	-- keymap('n', '<C-K>', lspfunc.signature_help, bufopts)
	keymap('n', '<space>wa', lspfunc.add_workspace_folder, bufopts)
	keymap('n', '<space>wr', lspfunc.remove_workspace_folder, bufopts)
	keymap('n', '<space>wl',
		function()
			print(vim.inspect(lspfunc.list_workspace_folders()))
		end, bufopts)
	keymap('n', '<space>D', lspfunc.type_definition, bufopts)
	keymap('n', '<space>rn', lspfunc.rename, bufopts)
	keymap('n', '<space>ca', lspfunc.code_action, bufopts)
	keymap('n', 'gr', lspfunc.references, bufopts)
	keymap('n', '<space>f', lspfunc.format, bufopts)

	-- See `:help vim.lsp.*` for documentation
end

-- Overide default configuration with the on attach configuration
-- local lspconfig = require("lspconfig")
-- lspconfig.util.default_config = vim.tbl_extend(
-- 	"force",
-- 	lspconfig.util.default_config,
-- 	{
-- 		on_attach = on_attach
-- 	}
-- )

-- Define cmp capabilities
local capabilities = require('cmp_nvim_lsp').default_capabilities()
-- local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

-- Automatically setup installed servers
if ok then
	_.setup_handlers({
		-- Default handler
		function(server_name)
			require("lspconfig")[server_name].setup {
				on_attach = on_attach,
				capabilities = capabilities,
			}
		end

		-- Other handlers server specific handlers...
	})
end
